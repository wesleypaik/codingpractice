def are_they_equal(array_a, array_b):
    # Write your code here
    # take first array and compare to the other, if corresponding iteration does not match try another, if a match can be made with existing for all iterations, then it is true
    """
    if set(array_a) == set(array_b):
      return True
    else:
      return False
    """
    if array_a == array_b:
        return True
    else:
        # send first array to bubble sort
        a = bubbleSort(array_a)
        # send second array to bubble sort
        b = bubbleSort(array_b)
        # compare arrays equality
        if a == b:
            return True
        else:
            return False


def bubbleSort(arr):
    n = len(arr)

    # Traverse through all array elements
    for i in range(n - 1):
        # range(n) also work but outer loop will repeat one time more than needed.

        # Last i elements are already in place
        for j in range(0, n - i - 1):
            # traverse the array from 0 to n-i-1
            # Swap if the element found is greater
            # than the next element
            if arr[j] > arr[j + 1]:
                arr[j], arr[j + 1] = arr[j + 1], arr[j]
    return (arr)


A = [1, 2, 3, 4]
B = [1, 4, 3, 2]
print(are_they_equal(A,B))
