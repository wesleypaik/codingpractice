# QuickSort is a Divide and Conquer algorithm. It picks an element as pivot and partitions
# the given array around the picked pivot. There are many different versions of quickSort that
# pick pivot in different ways.
#
# Always pick first element as pivot.
# Always pick last element as pivot (implemented below)
# Pick a random element as pivot.
# Pick median as pivot.
#
# The key process in quickSort is partition(). Target of partitions is, given an array and an
# element x of array as pivot, put x at its correct position in sorted array and put all
# smaller elements (smaller than x) before x, and put all greater elements (greater than x)
# after x. All this should be done in linear time.


def QuickSort(arr):
    elements = len(arr)

    # Base case
    if elements < 2:
        return arr

    current_position = 0  # Position of the partitioning element

    for i in range(1, elements):  # Partitioning loop
        if arr[i] <= arr[0]:
            current_position += 1
            temp = arr[i]
            arr[i] = arr[current_position]
            arr[current_position] = temp

    temp = arr[0]
    arr[0] = arr[current_position]
    arr[current_position] = temp  # Brings pivot to it's appropriate position

    left = QuickSort(arr[0:current_position])  # Sorts the elements to the left of pivot
    right = QuickSort(arr[current_position + 1:elements])  # sorts the elements to the right of pivot

    arr = left + [arr[current_position]] + right  # Merging everything together

    return arr


array_to_be_sorted = [4, 2, 7, 3, 1, 6]
print("Original Array: ", array_to_be_sorted)
print("Sorted Array: ", QuickSort(array_to_be_sorted))
