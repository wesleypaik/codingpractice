
# Lists are enclosed in brackets:
l = [1, 2, "a"]

# Tuples are enclosed in parentheses:
# Tuples are faster and consume less memory.
t = (1, 2, "a")

# Dictionaries are built with curly brackets:
d = {"a": 1, "b": 2}

# Sets are constructed from a sequence (or some other iterable object).
# Since sets cannot have duplications, they are usually used to build a sequence
# of unique items (e.g., set of identifiers).
a = set([1, 2, 3, 4])
b = set([3, 4, 5, 6])
print(a | b)  # Union
print(a & b)  # Intersection