"""
append()	Adds an element at the end of the list
clear()	    Removes all the elements from the list
copy()	    Returns a copy of the list
count()	    Returns the number of elements with the specified value
extend()	Add the elements of a list (or any iterable), to the end of the current list
index()	    Returns the index of the first element with the specified value
insert()	Adds an element at the specified position
pop()	    Removes the element at the specified position
remove()	Removes the first item with the specified value
reverse()	Reverses the order of the list
sort()	    Sorts the list
"""

# Create and array with items and access an item
thisList = ["apple", "banana", "cherry"]
print(thisList[1])

# Negative Indexing
thisList = ["apple", "banana", "cherry"]
print(thisList[-1])

# Range Index
thisList = ["apple", "banana", "cherry", "orange", "kiwi", "melon", "mango"]
print(thisList[2:5])

thisList = ["apple", "banana", "cherry", "orange", "kiwi", "melon", "mango"]
print(thisList[:4])

thisList = ["apple", "banana", "cherry", "orange", "kiwi", "melon", "mango"]
print(thisList[2:])

thisList = ["apple", "banana", "cherry"]
if "apple" in thisList:
    print("Yes, 'apple' is in the fruits list")

thisList = ["apple", "banana", "cherry"]
thisList[1] = "blackcurrant"
print(thisList)

thisList = ["apple", "banana", "cherry", "orange", "kiwi", "mango"]
thisList[1:3] = ["blackcurrant", "watermelon"]
print(thisList)

thisList = ["apple", "banana", "cherry"]
thisList[1:2] = ["blackcurrant", "watermelon"]
print(thisList)

thisList = ["apple", "banana", "cherry"]
thisList.insert(2, "watermelon")
print(thisList)

thisList = ["apple", "banana", "cherry"]
thisList.append("orange")
print(thisList)

thisList = ["apple", "banana", "cherry"]
tropical = ["mango", "pineapple", "papaya"]
thisList.extend(tropical)
print(thisList)

cars = ["Ford", "Volvo", "BMW"]
cars.append("Honda")
print(cars)
cars.pop(1)
print(cars)
cars.sort()
print(cars)


