
def rotationalCipher(inputs, rotation_factor):
    # Write your code here
    alphaArray = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j",
                  "k", "l", "m", "n", "o", "p", "q", "r",
                  "s", "t", "u", "v", "w", "x", "y", "z"]
    numArray = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]

    newArray = []
    i = 0
    kno = ""
    for chara in inputs:
        upper = False
        print(i, chara)
        upper = chara.isupper()
        chara = chara.lower()
        if chara in alphaArray:
            first_location = alphaArray.index(chara)
            second_location = (first_location + rotation_factor) % 26
            if upper:
                newUpper = alphaArray[second_location]
                newUpper = newUpper.upper()
                newArray.append(newUpper)
            else:
                newArray.append(alphaArray[second_location])
        elif chara in numArray:
            first_location = numArray.index(chara)
            second_location = (first_location + rotation_factor) % 10
            newArray.append(numArray[second_location])
        else:
            newArray.append(chara)
        i += 1
    for hee in newArray:
        kno = kno + hee
    return kno


print(rotationalCipher("abcdefghijklmNOPQRSTUVWXYZ0123456789", 39))
