from queue import Queue


class Node(object):
    def __init__(self, v):
        self.val = v
        self.left = None
        self.right = None


def _collect(node, data, depth = 0):
    if not node:
        return None

    if depth not in data:
        data[depth] = []

    data[depth].append(node.val)

    _collect(node.left, data, depth + 1)
    _collect(node.right, data, depth + 1)


def avg_by_depth(node):
    data = {}
    _collect(node, data)

    result = []

    i = 0
    while i in data:
        nums = data[i]
        avg = sum(nums) / len(nums)
        result.append(avg)
        i += 1

    return result


if __name__ == '__main__':
    # Let us construct a Binary Tree
    #   4
    #  / \
    #  7 9
    #  / \ \
    # 10  2 6
    #      \
    #       6
    #      /
    #     2
    #
    root = Node(4)
    root.left = Node(7)
    root.right = Node(9)
    root.left.left = Node(10)
    root.left.right = Node(2)
    root.right.right = Node(6)
    root.left.right.right = Node(6)
    root.left.right.right.left = Node(2)

    print(avg_by_depth(root))
